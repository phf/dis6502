#ifndef DIS6502_H
#define DIS6502_H

/*
 * dis6502 by Robert Bond, Udi Finkelstein, and Eric Smith
 *
 * $Id: dis.h 26 2004-01-17 23:28:23Z eric $
 * Copyright 2000-2003 Eric Smith <eric@brouhaha.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.  Note that permission is
 * not granted to redistribute this program under the terms of any
 * other version of the General Public License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */

#include <stdint.h>
#include <stdio.h>
#include <stdnoreturn.h>

extern int sevenbit; /* if true, mask character data with 0x7f to ignore MSB */

typedef uint16_t addr_t;

#define NPREDEF 10

extern char *predef[];
extern int npredef;
extern char *global_file;
extern char *progname;

extern int bopt;
enum boot_mode {
	UNKNOWN,
	RAW_BINARY,
	ATARI_LOAD,
	C64_LOAD,
	ATARI_BOOT,
};
extern int global_base_address, global_vector_address;

extern int asmout;
extern int debuglog;
extern unsigned char f[];
extern unsigned char d[];
extern long offset[];

#define getword(x) (d[x] + (d[x + 1] << 8))
#define getbyte(x) (d[x])

/* f bits */
enum {
	LOADED = (1 << 0), /* Location loaded */
	JREF = (1 << 1),   /* Referenced as jump/branch dest */
	DREF = (1 << 2),   /* Referenced as data */
	SREF = (1 << 3),   /* Referenced as subroutine dest */
	NAMED = (1 << 4),  /* Has a name */
	TDONE = (1 << 5),  /* Has been traced */
	ISOP = (1 << 6),   /* Is a valid instruction opcode */
	OFFSET = (1 << 7), /* should be printed as an offset */
};

struct mnemonic {
	char name[4]; /* three-letter mnemonic name */
	int length;   /* number of bytes */
	int flags;    /* control flow and addressing mode */
};

extern struct mnemonic optbl[];

/* Flags */

/* Where control goes */
enum {
	NORM = (1 << 0),
	JUMP = (1 << 1),
	FORK = (1 << 2),
	STOP = (1 << 3),
	CTLMASK = (NORM | JUMP | FORK | STOP),
};

/* Instruction format */
enum {
	IMM = (1 << 5),
	ABS = (1 << 6),
	ACC = (1 << 7),
	IMP = (1 << 8),
	INX = (1 << 9),
	INY = (1 << 10),
	ZPX = (1 << 11),
	ABX = (1 << 12),
	ABY = (1 << 13),
	REL = (1 << 14),
	IND = (1 << 15),
	ZPY = (1 << 16),
	ZPG = (1 << 17),
	ILL = (1 << 18),
	ADRMASK = (IMM | ABS | ACC | IMP | INX | INY | ZPX | ABX | ABY | REL | IND | ZPY | ZPG | ILL),
};

struct ref_chain {
	struct ref_chain *next;
	int who;
};

struct ref_chain *get_ref(addr_t loc);
char *get_name(addr_t loc);

/* lex junk */
enum {
	EQ = 256,
	NUMBER = 257,
	NAME = 258,
	COMMENT = 259,
	LI = 260,
	TSTART = 261,
	TSTOP = 262,
	TRTSTAB = 263,
	TJTAB2 = 264,
	EQS = 265,
	OFS = 266,
};

extern FILE *yyin;
int lineno;

typedef union {
	int ival;
	char *sval;
} VALUE;

extern VALUE token;

/* lex.c (scanner generated from lex.l) */
int yylex(void);

/* initopts.c */
void initopts(int argc, char *argv[]);

/* print.c */
void dumpitout(void);

/* ref.c */
void save_ref(addr_t refer, addr_t refee);
void save_name(addr_t loc, char *name);

/* queue.c */
void queue_init(void);
int queue_empty(void);
void queue_push(addr_t addr);
addr_t queue_pop(void);

/* main.c */
noreturn void crash(const char *fmt, ...) __attribute__((format(printf, 1, 2)));
void debug(const char *fmt, ...) __attribute__((format(printf, 1, 2)));
void *emalloc(size_t bytes) __attribute__((malloc));
void get_predef(void);

#endif /* !DIS6502_H */
